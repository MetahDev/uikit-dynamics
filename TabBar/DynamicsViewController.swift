//
//  DynamicsViewController.swift
//  TabBar
//
//  Created by Metah on 6/30/19.
//  Copyright © 2019 Automatization X Software. All rights reserved.
//

import UIKit

class DynamicsViewController: UIViewController {
    // MARK:- Properties
    private lazy var redView: UIView = {
        let v = UIView()
        v.backgroundColor = .red
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    private lazy var greenView: UIView = {
        let v = UIView()
        v.backgroundColor = .green
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    private var animator: UIDynamicAnimator!
    private var currentLocation: CGPoint!
    private var attachment: UIAttachmentBehavior!
    
    
    // MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(redView)
        view.addSubview(greenView)
        setLayout()
        setupAnimator()
    }
    
    private func setLayout() {
        redView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        greenView.frame = CGRect(x: 200, y: 0, width: 100, height: 100)
        view.backgroundColor = .black
    }
    
    private func setupAnimator() {
        animator = UIDynamicAnimator(referenceView: self.view)
        
        setGravity()
        setCollision()
        setBehavior()
        setBoxAttachment()
    }
    
    private func setGravity() {
        let gravity = UIGravityBehavior(items: [redView, greenView])
        let vector = CGVector(dx: 0.0, dy: 1.0)
        gravity.gravityDirection = vector
        
        animator.addBehavior(gravity)
    }
    
    private func setCollision() {
        // *
        let collision = UICollisionBehavior(items: [redView, greenView])
        collision.translatesReferenceBoundsIntoBoundary = true
        
        animator.addBehavior(collision)
    }
    
    private func setBehavior() {
        let behavior = UIDynamicItemBehavior(items: [redView, greenView])
        behavior.elasticity = 0.5
        
        animator.addBehavior(behavior)
    }
    
    private func setBoxAttachment() {
        // **
        let boxAttachment = UIAttachmentBehavior(item: greenView, attachedTo: redView)
        boxAttachment.frequency = 2.0
        
        animator.addBehavior(boxAttachment)
    }
    
    
    // MARK:- Attachment
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let theTouch = touches.first {
            // ***
            currentLocation = theTouch.location(in: self.view)
            let offset = UIOffset(horizontal: 20, vertical: 20)
            attachment = UIAttachmentBehavior(item: redView, offsetFromCenter: offset, attachedToAnchor: currentLocation)
            
            animator.addBehavior(attachment)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let theTouch = touches.first {
            currentLocation = theTouch.location(in: self.view)
            attachment.anchorPoint = currentLocation
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        animator.removeBehavior(attachment)
    }
}

/*
 * - items отталкиваются и от друг друга
 ** - greenView "привязан" к redView
 *** - мы говорим, что место нажатия это точка сцепления, что заставляет view "висеть" на некотором расстоянии, a offset делает притягивания реалистичнее
*/
