//
//  SnapViewController.swift
//  TabBar
//
//  Created by Metah on 6/30/19.
//  Copyright © 2019 Automatization X Software. All rights reserved.
//

import UIKit

class SnapViewController: UIViewController {
    // MARK:- Properties
    private lazy var innerView: UIView = {
        let v = UIView()
        v.backgroundColor = .blue
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    private var animator: UIDynamicAnimator!
    private var snapping: UISnapBehavior!
    
    
    // MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(innerView)
        setLayout()
        initGesture()
        setupAnimator()
    }
    
    private func setLayout() {
        innerView.frame.size = CGSize(width: 150, height: 150)
        innerView.center = view.center
        view.backgroundColor = .white
    }
    
    private func initGesture() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(moveView(_:)))
        innerView.isUserInteractionEnabled = true
        innerView.addGestureRecognizer(panGesture)
    }
    
    @objc private func moveView(_ recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            animator.removeBehavior(snapping)
        case .changed:
            let translation = recognizer.translation(in: view)
            innerView.center = CGPoint(x: innerView.center.x + translation.x, y: innerView.center.y + translation.y)
            recognizer.setTranslation(.zero, in: view)
        case .ended, .failed, .cancelled:
            animator.addBehavior(snapping)
        default:
            break
        }
    }

    private func setupAnimator() {
        animator = UIDynamicAnimator(referenceView: self.view)
        snapping = UISnapBehavior(item: innerView, snapTo: view.center)
        animator.addBehavior(snapping)
    }
}
