//
//  MenuViewController.swift
//  TabBar
//
//  Created by Metah on 6/30/19.
//  Copyright © 2019 Automatization X Software. All rights reserved.
//

import UIKit

class MenuViewController: UITabBarController {
    // MARK:- Properties
    private var info = ["blue": 1, "green": 0]
    
    
    // MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        configureTabBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTitle()
        setObservers()
    }
    
    private func configureTabBar() {
        // *
        let firstVC = SnapViewController()
        let firstTabBarItem = UITabBarItem(title: nil, image: UIImage(named: "1"), tag: 0)
        firstVC.tabBarItem = firstTabBarItem
        
        let secondVC = DynamicsViewController()
        let secondTabBarItem = UITabBarItem(title: nil, image: UIImage(named: "2"), tag: 1)
        secondVC.tabBarItem = secondTabBarItem
        
        let controllers = [firstVC, secondVC]
        self.viewControllers = controllers
    }
    
    private func setObservers() {
        // **
        NotificationCenter.default.addObserver(self, selector: #selector(didPressItem(_:)), name: .tabBarItemPressed, object: self)
    }
    
    private func updateTitle() {
        self.navigationItem.title = "1: \(self.info["blue"]!); 2: \(self.info["green"]!)"
    }
    
    
    // MARK:- Actions
    @objc private func didPressItem(_ notification: Notification) {
        if let data = notification.userInfo as? [String: Int] {
            self.info = data
            updateTitle()
        }
    }
}

extension MenuViewController: UITabBarControllerDelegate {
    // MARK:- UITabBarController
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        // ***
        if item.tag == 0 {
            self.info["blue"]! += 1
        } else {
            self.info["green"]! += 1
        }
        NotificationCenter.default.post(name: .tabBarItemPressed, object: self, userInfo: self.info)
    }
    
    // MARK:- UITabBarControllerDelegate
}

extension Notification.Name {
    // MARK:- Extensions
    static let tabBarItemPressed = Notification.Name("tabBarPressed")
}


// MARK:- Notes
/*
 О принципе работы приложения: экран с tabBar, где первое меню - синий экран, второй - зеленый. Когда происходит переключение, переменная info, которая хранит количество открываний, соответственно увеличивается. В консоль выходит сообщения со значениями переменной после каждого нажатия.
 
 * - Когда вы работаете в UITabBarController, вам необходимо лишь создать объекты UIViewController, между которыми пользователь будет переключаться и указать их tabBar, таким образом, даже при открытии этих вью(кстати UITabBarController контролирует навигацию сам), он будет оставаться и будет высвечиваться тот tabBarItem, который присвоен данному контроллеру(аргумент tag указывает последовательность). Ну, а дальше там по коду понятно.
 
 ** - NotificationCenter действует так: мы  говорим, что мы ждем какое-то  происшествие, создавая observer'а, ему указываем сначала класс, который за это отвечает, даем метод, который запустить потом, называем как это происшествие именуется(это Notification.Name, то есть мы создаем название, которое будет именовать это происшествие), и от какого объекта оно должно быть(nil - любой)

 *** А откуда знать когда запускать это происшествие? Это ведь не системный процесс: не нажатие кнопки или еще что-то. Поэтому, в нужном методе мы вызываем метод post, который сообщает NotificationCenter что произошло событие под названием X(и при желании, сказать какой объект его отправил и передать информацию), а дальше наконец observer вызывает методы.

 P.S: я специально сделал замудренный и нелогичный способ передачи информации, ради раскрытия темы
 
 P.P.S: Observers need to be removed before they are deallocated, or you risk sending a message to an object that doesn’t exist: zombie!
*/


